package me.alem.textwizard.services;

import me.alem.textwizard.domain.TextAnalysisRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Locale;

class TextAnalysisServiceTests {

    private TextAnalysisService service;

    @BeforeEach
    void beforeEach() {
        service = new TextAnalysisService();
    }

    @Test
    @DisplayName("Blank text produces an empty map")
    void testBlankTextProducesAnEmptyMap() {
        final var text = "";

        final var consonants = service.analyzeText(
                new TextAnalysisRequest(text, TextAnalysisRequest.Mode.Consonants)
        );

        Assertions.assertEquals(
                0,
                consonants.size(),
                "A blank text must produce an empty map when the mode used is consonants."
        );

        final var vowels = service.analyzeText(
                new TextAnalysisRequest(text, TextAnalysisRequest.Mode.Vowels)
        );

        Assertions.assertEquals(
                0,
                vowels.size(),
                "A blank text must produce an empty map when the mode used is vowels."
        );
    }

    @Test
    @DisplayName("Non alphabetic characters produce an empty map")
    void testNonAlphabeticCharactersProduceAnEmptyMap() {
        final var text = " \r\n\t  \uD83D\uDC6E\u200D\u2640\uFE0F öüäß 1234567890";

        final var consonants = service.analyzeText(
                new TextAnalysisRequest(text, TextAnalysisRequest.Mode.Consonants)
        );

        Assertions.assertEquals(
                0,
                consonants.size(),
                "A non-alphabetic text must produce an empty map when the mode used is consonants."
        );

        final var vowels = service.analyzeText(
                new TextAnalysisRequest(text, TextAnalysisRequest.Mode.Vowels)
        );

        Assertions.assertEquals(
                0,
                vowels.size(),
                "A non-alphabetic text must produce an empty map when the mode used is vowels."
        );
    }

    @Test
    @DisplayName("Text with only vowels")
    void testTextWithOnlyVowels() {
        final var text = "aeiouAEIOUaA";

        final var result = service.analyzeText(
                new TextAnalysisRequest(text, TextAnalysisRequest.Mode.Vowels)
        );

        Assertions.assertEquals(5, result.size(), "Expected to see five vowels.");

        Assertions.assertEquals(4, result.get('A'), "Expected four occurrences of the letter A.");
        Assertions.assertEquals(2, result.get('E'), "Expected two occurrences of the letter E.");
        Assertions.assertEquals(2, result.get('I'), "Expected two occurrences of the letter I.");
        Assertions.assertEquals(2, result.get('O'), "Expected two occurrences of the letter O.");
        Assertions.assertEquals(2, result.get('U'), "Expected two occurrences of the letter U.");
    }

    @Test
    @DisplayName("Text with only consonants")
    void testTextWithOnlyConsonants() {
        final var baseConsonants = "bcdfghjklmnpqrstvwxyz";
        final var text = baseConsonants + baseConsonants.toUpperCase(Locale.ENGLISH);

        final var result = service.analyzeText(
                new TextAnalysisRequest(text, TextAnalysisRequest.Mode.Consonants)
        );

        Assertions.assertEquals(21, result.size(), "Expected to see 21 consonants.");

        for (var i = 0; i < baseConsonants.length(); i++) {
            final var consonant = Character.toUpperCase(baseConsonants.charAt(i));

            Assertions.assertEquals(2, result.get(consonant), "Expected two occurrences of the letter " + consonant);
        }
    }

    @Test
    @DisplayName("Count vowels in mixed string")
    void testCountVowelsInMixedString() {
        final var text = "The quick brown fox jumps over the lazy dog.";

        final var result = service.analyzeText(
                new TextAnalysisRequest(text, TextAnalysisRequest.Mode.Vowels)
        );

        Assertions.assertEquals(5, result.size(), "Expected to see five vowels.");

        Assertions.assertEquals(1, result.get('A'), "Expected to see one occurrence of the letter 'A'.");
        Assertions.assertEquals(3, result.get('E'), "Expected to see three occurrences of the letter 'E'.");
        Assertions.assertEquals(1, result.get('I'), "Expected to see one occurrence of the letter 'I'.");
        Assertions.assertEquals(4, result.get('O'), "Expected to see four occurrences of the letter 'O'.");
        Assertions.assertEquals(2, result.get('U'), "Expected to see two occurrences of the letter 'U'.");
    }

    @Test
    @DisplayName("Count consonants in mixed string")
    void testCountConsonantsInMixedString() {
        final var text = "The quick brown fox jumps over the lazy dog.";

        final var result = service.analyzeText(
                new TextAnalysisRequest(text, TextAnalysisRequest.Mode.Consonants)
        );

        Assertions.assertEquals(21, result.size(), "Expected to see 21 consonants.");
    }
}
