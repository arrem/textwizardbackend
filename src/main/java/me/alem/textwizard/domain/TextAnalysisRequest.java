package me.alem.textwizard.domain;

/**
 * The <code>TextAnalysisRequest</code> is a record class that captures the information necessary to analyze a block
 * of text.
 *
 * @param text Represents the text that should be analyzed.
 * @param mode Represents the {@link Mode analysis mode} for the text.
 */
public record TextAnalysisRequest(String text, Mode mode) {
    /**
     * An enum that captures different supported modes of text analysis. Texts can be analyzed either by their
     * vowels, or their consonants.
     *
     * @see #Vowels
     * @see #Consonants
     */
    public enum Mode {
        /**
         * A text analysis mode that indicates that only the vowels of the given text should be counted.
         */
        Vowels,
        /**
         * A text analysis mode that indicates that only the consonants of the given text should be counted.
         */
        Consonants
    }
}
