package me.alem.textwizard.mappers;

import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * The <code>NotFoundExceptionMapper</code> handles instances of the {@link NotFoundException} that are thrown by
 * the application's resources. This exception is thrown when an API endpoint is queried for which no resource is
 * provided by the application.
 *
 * <p>
 * The response for this exception has the {@link Response.Status#NOT_FOUND not found} status.
 */
@Provider
public final class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {
    @Override
    public Response toResponse(NotFoundException exception) {
        return Response.status(Response.Status.NOT_FOUND)
                .type(MediaType.TEXT_PLAIN)
                .entity("The requested resource could not be found on the server.")
                .build();
    }
}
