package me.alem.textwizard.mappers;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * The <code>BadRequestExceptionMapper</code> handles instances of the {@link BadRequestException} that are thrown by
 * the application's resources. This exception is thrown when an invalid request is made and cannot be processed by the
 * server (e.g., because it is missing required parameters, or its parameters are malformed in some other way).
 *
 * <p>
 * The response for this exception has the {@link Response.Status#BAD_REQUEST bad request} status, as well as a textual
 * human-readable description of why the request could not be processed.
 */
@Provider
public final class BadRequestExceptionMapper implements ExceptionMapper<BadRequestException> {
    @Override
    public Response toResponse(BadRequestException exception) {
        final var message = "Bad request: '" + exception.getMessage() + "'";

        return Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.TEXT_PLAIN)
                .entity(message)
                .build();
    }
}
