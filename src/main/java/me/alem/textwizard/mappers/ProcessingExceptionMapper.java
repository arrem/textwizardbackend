package me.alem.textwizard.mappers;

import jakarta.ws.rs.ProcessingException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * The <code>ProcessingExceptionMapper</code> handles instances of the {@link ProcessingException} that are thrown by
 * the application's resources. This exception is thrown when the JSON body of a request is malformed.
 *
 * <p>
 * The response for this exception has the {@link Response.Status#BAD_REQUEST bad request} status, and contains a
 * textual description of why the JSON body could not be parsed correctly.
 */
@Provider
public final class ProcessingExceptionMapper implements ExceptionMapper<ProcessingException> {
    @Override
    public Response toResponse(ProcessingException exception) {
        final var message = "Invalid JSON request body: '" + exception.getCause().getMessage() + "'";

        return Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.TEXT_PLAIN)
                .entity(message)
                .build();
    }
}
