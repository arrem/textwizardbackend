package me.alem.textwizard.mappers;

import jakarta.ws.rs.NotAllowedException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * The <code>NotAllowedExceptionMapper</code> handles instances of the {@link NotAllowedException} that are thrown by
 * the application's resources. This exception is thrown when an API endpoint is queried with an HTTP method that is
 * not supported by the application.
 *
 * <p>
 * The response for this exception has the {@link Response.Status#METHOD_NOT_ALLOWED method not allowed} status.
 */
@Provider
public final class NotAllowedExceptionMapper implements ExceptionMapper<NotAllowedException> {
    @Override
    public Response toResponse(NotAllowedException exception) {
        return Response.status(Response.Status.METHOD_NOT_ALLOWED)
                .type(MediaType.TEXT_PLAIN)
                .entity("Method not allowed.")
                .build();
    }
}
