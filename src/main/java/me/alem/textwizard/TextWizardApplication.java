package me.alem.textwizard;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("/api")
public final class TextWizardApplication extends Application {

}
