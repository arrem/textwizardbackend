package me.alem.textwizard.services;

import jakarta.inject.Singleton;
import me.alem.textwizard.domain.TextAnalysisRequest;
import me.alem.textwizard.domain.TextAnalysisRequest.Mode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Unmodifiable;

import java.util.*;

/**
 * The <code>TextAnalysisService</code> is a service that provides means of counting the consonants and the
 * vowels in strings.
 *
 * @see #analyzeText(TextAnalysisRequest)
 */
@Singleton
public final class TextAnalysisService {
    private static final Set<Character> vowels = new HashSet<>(List.of('A', 'E', 'I', 'O', 'U'));

    /**
     * Analyzes a given text and computes the count of its vowels or consonants, as specified.
     *
     * @param request A non-nullable {@link TextAnalysisRequest} containing the text to be analyzed, as well as the
     *                {@link Mode} which should be used to analyze the text.
     *
     * @return An unmodifiable {@link Map} of characters of the specified type (either consonants or vowels) to their
     *         counts in the given text.
     */
    public @Unmodifiable Map<Character, Long> analyzeText(@NotNull TextAnalysisRequest request) {
        Objects.requireNonNull(request, "The text analysis request must not be null.");

        final var text = request.text();
        final var counts = new HashMap<Character, Long>();

        for (var i = 0; i < text.length(); i++) {
            final var character = Character.toUpperCase(text.charAt(i));

            // We want to add a character to our map if either of the following is true:
            //   1. The user requested to count vowels, and the character is a vowel.
            //   2. The user requested to count consonants, and the character is a consonant.
            final var countsAsVowel =
                    request.mode() == TextAnalysisRequest.Mode.Vowels && vowels.contains(character);

            final var countsAsConsonant =
                    request.mode() == TextAnalysisRequest.Mode.Consonants &&
                            character >= 'A' && character <= 'Z' &&
                            !vowels.contains(character);

            if (countsAsVowel || countsAsConsonant) {
                counts.merge(character, 1L, Long::sum);
            }
        }

        return Collections.unmodifiableMap(counts);
    }
}
