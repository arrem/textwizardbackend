package me.alem.textwizard.config;

import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.ext.Provider;

/**
 * The `CorsConfig` class sets up the application's global CORS config (meaning that it applies to all of its routes).
 * The configuration loads the {@link AppConfig#getFrontendURL()} property, and uses it as the sole allowed origin,
 * while allowing all methods and common headers.
 */
@Provider
public final class CorsConfig implements ContainerResponseFilter {

    @Inject
    private AppConfig appConfig;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        final var frontendURL = appConfig.getFrontendURL();
        final var origin = requestContext.getHeaderString("origin");

        if (origin == null || !origin.equals(frontendURL)) {
            return;
        }

        responseContext.getHeaders().add(
                "Access-Control-Allow-Origin",
                frontendURL
        );

        responseContext.getHeaders().add(
                "Access-Control-Allow-Headers",
                "origin, content-type, accept, authorization"
        );

        responseContext.getHeaders().add(
                "Access-Control-Allow-Methods",
                "*"
        );
    }
}
