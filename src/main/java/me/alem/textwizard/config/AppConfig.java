package me.alem.textwizard.config;

import jakarta.inject.Singleton;

import java.io.IOException;
import java.util.Properties;

/**
 * The `AppConfig` class is a singleton bean that manages the application's config. It attempts to scan the
 * application resources to find an {@value PROPERTIES_FILE_NAME} file in the application's resources, from which it
 * reads and makes available the configured application properties.
 */
@Singleton
public final class AppConfig {
    private static final String PROPERTIES_FILE_NAME = "/application.properties";

    private final Properties properties = new Properties();

    public AppConfig() throws IOException {
        final var loader = Thread.currentThread().getContextClassLoader();

        try (final var stream = loader.getResourceAsStream(PROPERTIES_FILE_NAME)) {
            properties.load(stream);
        }
    }

    /**
     * Returns the URL of the frontend for this application. This is used to correctly configure CORS for incoming
     * requests from the frontend.
     */
    public String getFrontendURL() {
        return properties.getProperty(PropertyNames.FrontendURL.key);
    }

    private enum PropertyNames {
        FrontendURL("frontend-url");

        private final String key;

        PropertyNames(String key) {
            this.key = key;
        }
    }
}
