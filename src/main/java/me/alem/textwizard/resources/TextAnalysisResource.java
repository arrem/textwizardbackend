package me.alem.textwizard.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import me.alem.textwizard.domain.TextAnalysisRequest;
import me.alem.textwizard.services.TextAnalysisService;

import java.util.Map;

/**
 * The <code>TextAnalysisResource</code> provides an API endpoint for counting vowels and consonants in a text.
 */
@Path("/analyze")
public final class TextAnalysisResource {
    @Inject
    private TextAnalysisService textAnalysisService;

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Map<Character, Long> analyze(TextAnalysisRequest body) {
        if (body == null) {
            throw new BadRequestException("This endpoint requires a text analysis request as its body.");
        }

        return textAnalysisService.analyzeText(body);
    }
}
