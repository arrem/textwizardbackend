# TextWizard Backend

This project contains the backend for the TextWizard application, written in Jakarta EE (formerly Java EE) and Java 19.

This file contains some guidelines for how to best read the project.

## The Analysis Service

The text analysis service is implemented in `me.alem.textwizard.services.TextAnalysisService`. The singleton bean provides just a single function, named `analyzeText` that processes a `me.alem.textwizard.domain.TextAnalysisRequest`, which is merely a DTO capturing a text and whether the user wants to count consonants or vowels.

For safety and static verification, the function uses annotations from the [JetBrains Annotations library](https://github.com/JetBrains/java-annotations), which allow IntelliJ to reason about properties that Java does not make explicit, such as (non-)nullability or (un-)modifiability.

Based on the original function having `a`, `e`, `i`, `o`, `u` as its vowels, this function also assumes that it is designed to work with the English alphabet, and will only recognize consonants from the English alphabet as valid consonants. This deliberate design choice is a step away from the original specification, which defined "consonant" to mean "anything that is not a vowel (including spaces and special characters)".

## The REST Resource

The other component of this backend is the REST(-like) API used to access the analysis service. The (very simple) implementation of this resource can be found in `me.alem.textwizard.resources.TextAnalysisResource`.

The resource simply verifies that the body has been provided, and invokes the analysis service, which is injected as a dependency.

Due to the fact that this application is just a backend API, to allow for machine-friendly error handling, Jakarta EE's default error mappers have been overridden with custom implementations that rely solely on HTTP status codes (and optional helpful messages), instead of returning HTML pages in case of an error. These handlers can be found in he `me.alem.textwizard.mappers` package.

## Tests

As required in the class-level comment of the reference implementation, the text analysis service was unit-tested using JUnit 5. The concrete class can be found in project's `src/test/` directory in the `me.alem.textwizard.services` package.